/* Copyright (C) 2011-2016 Micael Oliveira <micael.oliveira@mpsd.mpg.de>
 *                         Yann Pouillon <devops@materialsevolution.es>
 *
 * This file is part of Libpspio.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * version 2.0. If a copy of the MPL was not distributed with this file, You
 * can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Libpspio is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the Mozilla Public License version 2.0 for
 * more details.
 */

/**
 * @file pspio_meshfunc.h
 * @brief header file for the handling of functions defined on a mesh
 */

#ifndef PSPIO_MESHFUNC_H
#define PSPIO_MESHFUNC_H

#include "pspio_error.h"
#include "pspio_mesh.h"


/**********************************************************************
 * Data structures                                                    *
 **********************************************************************/

/**
* Mesh function structure
*/
typedef struct{
  pspio_mesh_t *mesh;    /**< Pointer to mesh */

  /* Functions */
  double *f[PSPIO_MAX_DERIVS+1];            /**< function values and derivatives on the mesh */

} pspio_meshfunc_t;


/**********************************************************************
 * Global routines                                                    *
 **********************************************************************/

/**
 * Allocates memory and preset function structure
 * 
 * @param[in,out] func: function structure
 * @param[in] np: number of points
 * @return error code
 * @note np should be larger than 1.
 */
int pspio_meshfunc_alloc(pspio_meshfunc_t **func, int np);

/**
 * Initializes the function data.
 * @param[in,out] func: function structure to be initialized.
 * @param[in] mesh: mesh structure.
 * @param[in] f: values of the function on the mesh.
 * @return error code
 * @note The func pointer has to be allocated first with the 
 *       pspio_meshfunc_alloc method.
 */
int pspio_meshfunc_init(pspio_meshfunc_t *func, const pspio_mesh_t *mesh, const double *f);

/**
 * Duplicates a mesh function structure
 * 
 * @param[out] dst: destination mesh function structure pointer
 * @param[in] src: source mesh function structure pointer
 * @return error code
 * @note The src pointer has to be allocated first with the pspio_meshfunc_alloc 
 *       method.
 * @note The dst pointer might or might not be allocated. If it is not, then it
 *       is allocate here.
 */
int pspio_meshfunc_copy(pspio_meshfunc_t **dst, const pspio_meshfunc_t *src);

/**
 * Frees all memory associated with function structure
 * 
 * @param[in,out] func: function structure
 * @note This function can be safely called even if some or all of the func 
 *       components have not been allocated.
 */
void pspio_meshfunc_free(pspio_meshfunc_t *func);


/**********************************************************************
 * Getters                                                            *
 **********************************************************************/

/**
 * Returns a pointer to the function.
 * 
 * @param[in] func: function structure
 * @return pointer to the function
 */
const double *pspio_meshfunc_get_function(const pspio_meshfunc_t *func);

/**
 * Returns a pointer to the nth derivative of the function.
 * 
 * @param[in] func: function structure
 * @param[in] n: order of the derivative (0 for function)
 * @return pointer to the first derivative
 */
const double *pspio_meshfunc_get_deriv(const pspio_meshfunc_t *func, int n);

/**
 * Returns a pointer to the mesh.
 * 
 * @param[in] func: function structure
 * @return pointer to the mesh
 */
const pspio_mesh_t *pspio_meshfunc_get_mesh(const pspio_meshfunc_t *func);


/**********************************************************************
 * Setters                                                            *
 **********************************************************************/

/**
 * Returns a pointer to the nth derivative of the function.
 * 
 * @param[in] func: function structure
 * @param[in] f: values of the function and derivatives on the mesh.
 * @param[in] n: order of the derivative (n > 1)
 * @return error code
 */
int pspio_meshfunc_set_deriv(pspio_meshfunc_t *func, const double *f, int n);


/**********************************************************************
 * Utility routines                                                   *
 **********************************************************************/

/**
 * Compares two mesh functions.
 * @param[in] meshfunc1: first mesh function to compare
 * @param[in] meshfunc2: second mesh function to compare
 * @return PSPIO_EQUAL when equal, PSPIO_DIFF when different, PSPIO_ERROR if a
 * problem occured.
 */
int pspio_meshfunc_cmp(const pspio_meshfunc_t *meshfunc1, const
                       pspio_meshfunc_t *meshfunc2);

#endif
