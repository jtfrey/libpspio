/* -*- mode: C; c-basic-offset: 2 -*- */
/* Copyright (C) 2021 Damien Caliste <damien.caliste@cea.fr>
 *
 * This file is part of Libpspio.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * version 2.0. If a copy of the MPL was not distributed with this file, You
 * can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Libpspio is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the Mozilla Public License version 2.0 for
 * more details.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include "input.h"
#include "pspio_common.h"
#include "pspio_error.h"

typedef enum {
  INPUT_FILE,
  INPUT_FILE_DESCRIPTOR,
  INPUT_MEMORY_BUFFER
} input_source;

struct _input_t {
  input_source source;
  union {
    FILE *file;
    int fd;
    char *buffer;
  } data;

  union {
    long at;
    off_t offset;
    char *saveptr;
  } position;

  char *saveptr;
  char line[PSPIO_STRLEN_LINE];
};

static int _init(input_t **input)
{
  assert(input != NULL);
  assert(*input == NULL);
  
  *input = (input_t*)malloc(sizeof(input_t));
  FULFILL_OR_RETURN(*input != NULL, PSPIO_ENOMEM);
  memset(*input, '\0', sizeof(input_t));

  return PSPIO_SUCCESS;
}

int input_new_from_filename(input_t **input, const char *filename)
{
  SUCCEED_OR_RETURN( _init(input) );

  (*input)->data.file = fopen(filename, "r");
  FULFILL_OR_RETURN((*input)->data.file != NULL, PSPIO_ENOFILE);

  (*input)->source = INPUT_FILE;

  return PSPIO_SUCCESS;
}

int input_new_from_file_descriptor(input_t **input, int fd)
{
  SUCCEED_OR_RETURN( _init(input) );

  (*input)->data.fd = fd;
  (*input)->source = INPUT_FILE_DESCRIPTOR;

  return PSPIO_SUCCESS;
}

int input_new_from_buffer(input_t **input, const char *buffer)
{
  size_t len;

  SUCCEED_OR_RETURN( _init(input) );

  len = strlen(buffer) + 1;
  (*input)->data.buffer = (char*)malloc(sizeof(char) * (len + 1));
  FULFILL_OR_RETURN((*input)->data.buffer != NULL, PSPIO_ENOFILE);
  memcpy((*input)->data.buffer, buffer, len);
  (*input)->data.buffer[len] = '\0';
  (*input)->saveptr = (*input)->data.buffer;

  (*input)->source = INPUT_MEMORY_BUFFER;

  return PSPIO_SUCCESS;
}

#define RETURN_ON_READ_FAILURE(N)               \
  if (N < 0)                                    \
    {                                           \
      fprintf(stderr, "%s\n", strerror(errno)); \
      return NULL;                              \
    }
char* input_get_line(input_t *input)
{
  assert(input != NULL);

  switch (input->source)
    {
    case INPUT_FILE:
      input->line[0] = '\0';
      return fgets(input->line, PSPIO_STRLEN_LINE, input->data.file) ? input->line : NULL;
    case INPUT_FILE_DESCRIPTOR:
      {
        size_t offset, bufLn;
        ssize_t readLn;

        if (input->saveptr)
          {
            /* Remove from the buffer the previously read line. */
            memmove(input->line, input->saveptr + 1,
                    PSPIO_STRLEN_LINE - (input->saveptr - input->line + 1));
            input->saveptr = strchr(input->line, '\n');
            if (input->saveptr)
              {
                *input->saveptr = '\0';
                return input->line;
              }
          }
        else
          input->line[0] = '\0';
        offset = strlen(input->line);
        bufLn = PSPIO_STRLEN_LINE - offset - 1;
        do
          {
            readLn = read(input->data.fd, input->line + offset, bufLn);
            RETURN_ON_READ_FAILURE(readLn);
            offset += readLn;
            bufLn -= readLn;
          } while (bufLn > 0 && readLn > 0);
        input->line[offset] = '\0';
        input->saveptr = strchr(input->line, '\n');
        if (input->saveptr)
          *input->saveptr = '\0';
        return *input->line ? input->line : NULL;
      }
    case INPUT_MEMORY_BUFFER:
      {
        char *out = NULL;
        char *nl = strchr(input->saveptr, '\n');
        if (nl)
          {
            *nl = '\0';
            out = input->saveptr;
            input->saveptr = nl + 1;
          }
        else if (*input->saveptr)
          {
            out = input->saveptr;
            input->saveptr += strlen(input->saveptr);
          }
        return out;
      }
    }
  return NULL;
}

void input_push_position(input_t *input)
{
  assert(input != NULL);

  switch (input->source)
    {
    case INPUT_FILE:
      input->position.at = ftell(input->data.file);
      break;
    case INPUT_FILE_DESCRIPTOR:
      input->position.offset = lseek(input->data.fd, 0, SEEK_CUR);
      break;
    case INPUT_MEMORY_BUFFER:
      input->position.saveptr = input->saveptr;
      break;
    default:
      break;
    }
}

void input_pop_position(input_t *input)
{
  assert(input != NULL);

  switch (input->source)
    {
    case INPUT_FILE:
      fseek(input->data.file, input->position.at, SEEK_SET);
      break;
    case INPUT_FILE_DESCRIPTOR:
      lseek(input->data.fd, input->position.offset, SEEK_SET);
      break;
    case INPUT_MEMORY_BUFFER:
      input->saveptr = input->position.saveptr;
      break;
    default:
      break;
    }
}

void input_rewind(input_t *input)
{
  assert(input != NULL);

  switch (input->source)
    {
    case INPUT_FILE:
      fseek(input->data.file, 0, SEEK_SET);
      break;
    case INPUT_FILE_DESCRIPTOR:
      lseek(input->data.fd, 0, SEEK_SET);
      break;
    case INPUT_MEMORY_BUFFER:
      input->saveptr = input->data.buffer;
      break;
    default:
      break;
    }
}

void input_free(input_t *input)
{
  assert(input != NULL);

  switch (input->source)
    {
    case INPUT_FILE:
      fclose(input->data.file);
      break;
    case INPUT_MEMORY_BUFFER:
      free(input->data.buffer);
      break;
    default:
      break;
    }
  free(input);
}
