/* Copyright (C) 2015-2016 Micael Oliveira <micael.oliveira@mpsd.mpg.de>
 *                         Yann Pouillon <devops@materialsevolution.es>
 *
 * This file is part of Libpspio.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * version 2.0. If a copy of the MPL was not distributed with this file, You
 * can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Libpspio is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the Mozilla Public License version 2.0 for
 * more details.
 */

/**
 * @file check_pspio_meshfunc.c
 * @brief checks pspio_meshfunc.c and pspio_meshfunc.h 
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <check.h>

#include "pspio_error.h"
#include "pspio_mesh.h"
#include "pspio_meshfunc.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#if defined HAVE_GSL
#define TOL_FUNC 1.0e-10
#else
#define TOL_FUNC 1.0e-6
#endif


static pspio_mesh_t *m1 = NULL, *m2 = NULL;
static pspio_meshfunc_t *mf11 = NULL, *mf12 = NULL, *mf2 = NULL;
static double *f11;
static double *f12;
static double *f2;

void meshfunc_setup(void)
{
  int i;
  const double *r2;
  const double r1[] = {0.0, 0.05, 0.10, 0.20, 0.40, 0.65, 0.85, 1.00};
  const double r1ab[] = {0.05, 0.05, 0.20, 0.20, 0.20, 0.20, 0.05, 0.05};
  const double a = 1.0;
  const double b = 2.0;

  pspio_mesh_free(m1);
  pspio_mesh_alloc(&m1, 8);
  pspio_mesh_init(m1, PSPIO_MESH_UNKNOWN, a, b, r1, r1ab);

  pspio_mesh_free(m2);
  pspio_mesh_alloc(&m2, 6);
  pspio_mesh_init_from_parameters(m2, PSPIO_MESH_LOG1, a, b);
  r2 = pspio_mesh_get_r(m2);

  pspio_meshfunc_free(mf11);
  pspio_meshfunc_free(mf12);
  pspio_meshfunc_free(mf2);
  pspio_meshfunc_alloc(&mf11, 8);
  pspio_meshfunc_alloc(&mf12, 8);
  pspio_meshfunc_alloc(&mf2, 6);

  f11 = (double *)malloc(8*sizeof(double));
  for (i=0; i<8; i++) {
    f11[i] = r1[i]*r1[i];
  }
  f12 = (double *)malloc(8*sizeof(double));
  for (i=0; i<8; i++) {
    f12[i] = r1[i]*r1[i]*r1[i];
  }
  f2 = (double *)malloc(6*sizeof(double));
  for (i=0; i<pspio_mesh_get_np(m2); i++) {
    f2[i] = r2[i]*r2[i];
  }

}

void meshfunc_teardown(void)
{
  pspio_mesh_free(m1);
  pspio_mesh_free(m2);
  m1 = NULL;
  m2 = NULL;

  pspio_meshfunc_free(mf11);
  pspio_meshfunc_free(mf12);
  pspio_meshfunc_free(mf2);
  mf11 = NULL;
  mf12 = NULL;
  mf2 = NULL;

  free(f11);
  free(f12);
  free(f2);
}


START_TEST(test_meshfunc_alloc)
{
  ck_assert(pspio_meshfunc_alloc(&mf11, 3) == PSPIO_SUCCESS);
}
END_TEST

START_TEST(test_meshfunc_init1)
{
  ck_assert(pspio_meshfunc_init(mf11, m1, f11) == PSPIO_SUCCESS);
}
END_TEST

START_TEST(test_meshfunc_set_deriv)
{
  pspio_meshfunc_init(mf11, m1, f11);
  ck_assert(pspio_meshfunc_set_deriv(mf11, f12, 1) == PSPIO_SUCCESS);

}
END_TEST

START_TEST(test_meshfunc_unset_deriv)
{
  pspio_meshfunc_init(mf11, m1, f11);
  ck_assert(pspio_meshfunc_set_deriv(mf11, f12, 1) == PSPIO_SUCCESS);
  ck_assert(pspio_meshfunc_get_deriv(mf11, 1));
  ck_assert(pspio_meshfunc_set_deriv(mf11, NULL, 1) == PSPIO_SUCCESS);
  ck_assert(!pspio_meshfunc_get_deriv(mf11, 1));
}
END_TEST

START_TEST(test_meshfunc_get_deriv)
{
  int i;
  const double *f;

  pspio_meshfunc_init(mf11, m1, f11);
  pspio_meshfunc_set_deriv(mf11, f12, 1);

  f = pspio_meshfunc_get_deriv(mf11, 1);
  for (i=0; i<pspio_mesh_get_np(m1); i++) {
    ck_assert(f[i] == f12[i]);
  }

}
END_TEST

START_TEST(test_meshfunc_cmp_equal)
{
  pspio_meshfunc_init(mf11, m1, f11);
  pspio_meshfunc_init(mf12, m1, f11);
  ck_assert(pspio_meshfunc_cmp(mf11, mf12) == PSPIO_EQUAL);
}
END_TEST

START_TEST(test_meshfunc_cmp_diff_mesh)
{ pspio_meshfunc_init(mf11, m1, f11);
  pspio_meshfunc_init(mf2, m2, f2);
  ck_assert(pspio_meshfunc_cmp(mf11, mf2) == PSPIO_DIFF);
}
END_TEST

START_TEST(test_meshfunc_cmp_diff_f)
{
  pspio_meshfunc_init(mf11, m1, f11);
  pspio_meshfunc_init(mf12, m1, f12);
  ck_assert(pspio_meshfunc_cmp(mf11, mf12) == PSPIO_DIFF);
}
END_TEST

START_TEST(test_meshfunc_cmp_diff_fp)
{
  pspio_meshfunc_init(mf11, m1, f11);
  pspio_meshfunc_init(mf12, m1, f11);
  pspio_meshfunc_set_deriv(mf11, f11, 1);
  pspio_meshfunc_set_deriv(mf12, f12, 1);
  ck_assert(pspio_meshfunc_cmp(mf11, mf12) == PSPIO_DIFF);
}
END_TEST

START_TEST(test_meshfunc_cmp_diff_fp2)
{
  pspio_meshfunc_init(mf11, m1, f11);
  pspio_meshfunc_init(mf12, m1, f11);
  pspio_meshfunc_set_deriv(mf11, f11, 1);
  ck_assert(pspio_meshfunc_cmp(mf11, mf12) == PSPIO_MTEQUAL);
}
END_TEST


START_TEST(test_meshfunc_copy_null)
{
  pspio_meshfunc_init(mf11, m1, f11);
  pspio_meshfunc_set_deriv(mf11, f11, 1);
  pspio_meshfunc_free(mf12);
  mf12 = NULL;
  ck_assert(pspio_meshfunc_copy(&mf12, mf11) == PSPIO_SUCCESS);
  ck_assert(pspio_meshfunc_cmp(mf11, mf12) == PSPIO_EQUAL);
}
END_TEST

START_TEST(test_meshfunc_copy_nonnull)
{
  pspio_meshfunc_init(mf11, m1, f11);
  pspio_meshfunc_init(mf12, m1, f12);
  ck_assert(pspio_meshfunc_copy(&mf12, mf11) == PSPIO_SUCCESS);
  ck_assert(pspio_meshfunc_cmp(mf11, mf12) == PSPIO_EQUAL);
}
END_TEST

START_TEST(test_meshfunc_copy_nonnull_size)
{
  pspio_meshfunc_init(mf11, m1, f11);
  pspio_meshfunc_init(mf2, m2, f2);
  ck_assert(pspio_meshfunc_copy(&mf2, mf11) == PSPIO_SUCCESS);
  ck_assert(pspio_meshfunc_cmp(mf11, mf2) == PSPIO_EQUAL);
}
END_TEST

START_TEST(test_meshfunc_get_func)
{
  int i;
  const double *f;

  pspio_meshfunc_init(mf11, m1, f11);

  f = pspio_meshfunc_get_function(mf11);
  for (i=0; i<pspio_mesh_get_np(m1); i++) {
    ck_assert(f[i] == f11[i]);
  }
}
END_TEST

START_TEST(test_meshfunc_get_mesh)
  {
    pspio_meshfunc_init(mf11, m1, f11);

    ck_assert(pspio_mesh_cmp(pspio_meshfunc_get_mesh(mf11), m1) == PSPIO_EQUAL);
  }
END_TEST


Suite * make_meshfunc_suite(void)
{
  Suite *s;
  TCase *tc_alloc, *tc_init, *tc_cmp, *tc_copy, *tc_get, *tc_deriv;

  s = suite_create("Mesh function");

  tc_alloc = tcase_create("Allocation");
  tcase_add_checked_fixture(tc_alloc, NULL, meshfunc_teardown);
  tcase_add_test(tc_alloc, test_meshfunc_alloc);
  suite_add_tcase(s, tc_alloc);

  tc_init = tcase_create("Initialization");
  tcase_add_checked_fixture(tc_init, meshfunc_setup, meshfunc_teardown);
  tcase_add_test(tc_init, test_meshfunc_init1);
  suite_add_tcase(s, tc_init);

  tc_deriv = tcase_create("Derivatives");
  tcase_add_checked_fixture(tc_deriv, meshfunc_setup, meshfunc_teardown);
  tcase_add_test(tc_deriv, test_meshfunc_set_deriv);
  tcase_add_test(tc_deriv, test_meshfunc_get_deriv);
  tcase_add_test(tc_deriv, test_meshfunc_unset_deriv);
  suite_add_tcase(s, tc_deriv);

  tc_cmp = tcase_create("Comparison");
  tcase_add_checked_fixture(tc_cmp, meshfunc_setup, meshfunc_teardown);
  tcase_add_test(tc_cmp, test_meshfunc_cmp_equal);
  tcase_add_test(tc_cmp, test_meshfunc_cmp_diff_mesh);
  tcase_add_test(tc_cmp, test_meshfunc_cmp_diff_f);
  tcase_add_test(tc_cmp, test_meshfunc_cmp_diff_fp);
  tcase_add_test(tc_cmp, test_meshfunc_cmp_diff_fp2);
  suite_add_tcase(s, tc_cmp);

  tc_copy = tcase_create("Copy");
  tcase_add_checked_fixture(tc_copy, meshfunc_setup, meshfunc_teardown);
  tcase_add_test(tc_copy, test_meshfunc_copy_null);
  tcase_add_test(tc_copy, test_meshfunc_copy_nonnull);
  tcase_add_test(tc_copy, test_meshfunc_copy_nonnull_size);
  suite_add_tcase(s, tc_copy);

  tc_get = tcase_create("Getters");
  tcase_add_checked_fixture(tc_get, meshfunc_setup, meshfunc_teardown);
  tcase_add_test(tc_get, test_meshfunc_get_func);
  tcase_add_test(tc_get, test_meshfunc_get_mesh);
  suite_add_tcase(s, tc_get);

  return s;
}
