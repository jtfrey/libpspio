/* Copyright (C) 2011-2016 Joseba Alberdi <alberdi@hotmail.es>
 *                         Matthieu Verstraete <matthieu.jean.verstraete@gmail.com>
 *                         Micael Oliveira <micael.oliveira@mpsd.mpg.de>
 *                         Yann Pouillon <devops@materialsevolution.es>
 *
 * This file is part of Libpspio.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * version 2.0. If a copy of the MPL was not distributed with this file, You
 * can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Libpspio is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the Mozilla Public License version 2.0 for
 * more details.
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "pspio_meshfunc.h"
#include "util.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif


/**********************************************************************
 * Global routines                                                    *
 **********************************************************************/

int pspio_meshfunc_alloc(pspio_meshfunc_t **meshfunc, int np)
{
  assert(meshfunc != NULL);
  assert(*meshfunc == NULL);
  assert(np > 1);

  ALLOC_OR_RETURN( meshfunc );
  DEFER_FUNC_ERROR( pspio_mesh_alloc(&(*meshfunc)->mesh, np) );
  DEFER_ALLOC_ERROR( (*meshfunc)->f[0], double, np );
  CLEAN_AND_RETURN_ON_DEFERRED_ERROR( meshfunc );

  memset((*meshfunc)->f[0], 0, np*sizeof(double));

  return PSPIO_SUCCESS;
}

int pspio_meshfunc_init(pspio_meshfunc_t *func, const pspio_mesh_t *mesh, const double *f)
{
  assert(func != NULL);
  assert(func->f != NULL);
  assert(mesh != NULL);

  /* Copy mesh */
  SUCCEED_OR_RETURN( pspio_mesh_copy(&func->mesh, mesh) );

  /* Function */
  memcpy(func->f[0], f, mesh->np * sizeof(double));
  
  return PSPIO_SUCCESS;
}

int pspio_meshfunc_copy(pspio_meshfunc_t **dst, const pspio_meshfunc_t *src)
{
  int n, np;

  assert(src != NULL);
  assert(dst != NULL);

  np = pspio_mesh_get_np(src->mesh);

  if ( *dst == NULL ) {
    SUCCEED_OR_RETURN( pspio_meshfunc_alloc(dst, np) )
  }

  /* The mesh of the destination function must have the same number of points as the mesh of the source function */
  if ( pspio_mesh_get_np((*dst)->mesh) != np ) {
    pspio_meshfunc_free(*dst);
    *dst = NULL;
    SUCCEED_OR_RETURN(pspio_meshfunc_alloc(dst, np));
  }

  SUCCEED_OR_RETURN( pspio_mesh_copy(&(*dst)->mesh, src->mesh) );

  for(n=0; n<PSPIO_MAX_DERIVS+1; n++) {
    if ( src->f[n] != NULL ) {
      if ( (*dst)->f[n] == NULL ) {
        (*dst)->f[n] = (double *) malloc ((*dst)->mesh->np * sizeof(double));
      }
      memcpy((*dst)->f[n], src->f[n], np * sizeof(double));
    } else {
      (*dst)->f[n] = NULL;
    }
  }
  
  return PSPIO_SUCCESS;
}

void pspio_meshfunc_free(pspio_meshfunc_t *func)
{
  int n;

  if (func != NULL) {
    pspio_mesh_free(func->mesh);

    for(n=0; n<PSPIO_MAX_DERIVS+1; n++) {
      if (func->f[n] != NULL) free(func->f[n]);
    }
    free(func);
  }
}


/**********************************************************************
 * Getters                                                            *
 **********************************************************************/

const double *pspio_meshfunc_get_function(const pspio_meshfunc_t *func)
{
  assert(func != NULL);

  return func->f[0];
}

const double *pspio_meshfunc_get_deriv(const pspio_meshfunc_t *func, int n)
{
  assert(func != NULL);

  return func->f[n];
}


const pspio_mesh_t *pspio_meshfunc_get_mesh(const pspio_meshfunc_t *func)
{
  assert(func != NULL);

  return func->mesh;
}

/**********************************************************************
 * Setters                                                            *
 **********************************************************************/

int pspio_meshfunc_set_deriv(pspio_meshfunc_t *func, const double *f, int n) {

  assert(func != NULL);
  assert(n>0);

  if (f == NULL) {
    free(func->f[n]);
    func->f[n] = NULL;
  } else {
    if ( func->f[n] == NULL ) {
      func->f[n] = (double *) malloc (func->mesh->np * sizeof(double));
    }
    FULFILL_OR_RETURN(func->f[n] != NULL, PSPIO_ENOMEM);

    memcpy(func->f[n], f, func->mesh->np * sizeof(double));
  }

  return PSPIO_SUCCESS;
}


/**********************************************************************
 * Utility routines                                                   *
 **********************************************************************/

int pspio_meshfunc_cmp(const pspio_meshfunc_t *meshfunc1,
                       const pspio_meshfunc_t *meshfunc2)
{
  int i, n;
  int f_present[PSPIO_MAX_DERIVS+1];
  int any_exclusive=0;

  assert(meshfunc1 != NULL);
  assert(meshfunc2 != NULL);

  for(n=0; n<PSPIO_MAX_DERIVS+1; n++) {
    f_present[n]   = (meshfunc1->f[n] != NULL) && (meshfunc2->f[n] != NULL);
    any_exclusive  = any_exclusive || (meshfunc1->f[n] != NULL) != (meshfunc2->f[n] != NULL);
  }

  if ( pspio_mesh_cmp(meshfunc1->mesh, meshfunc2->mesh) == PSPIO_EQUAL ) {

    for (n=0; n<PSPIO_MAX_DERIVS+1; n++) {
      if (f_present[n]) {
        for (i=0; i<meshfunc1->mesh->np; i++) {
          if ( (meshfunc1->f[n])[i] != (meshfunc2->f[n])[i] ) return PSPIO_DIFF;
        }
      }
    }
    if (any_exclusive) return PSPIO_MTEQUAL;
    return PSPIO_EQUAL;
  } else {
    return PSPIO_DIFF;
  }
}
