/* Copyright (C) 2011-2016 Micael Oliveira <micael.oliveira@mpsd.mpg.de>
 *                         Yann Pouillon <devops@materialsevolution.es>
 *
 * This file is part of Libpspio.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * version 2.0. If a copy of the MPL was not distributed with this file, You
 * can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Libpspio is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the Mozilla Public License version 2.0 for
 * more details.
 */

/**
 * @file check_pspio_error.c
 * @brief checks pspio_error.c and pspio_error.h 
 */

#include <stdio.h>
#include <check.h>
#include <sys/types.h>
#include <unistd.h>

#include "pspio_error.h"

static char *err_str;

void error_setup(void)
{
  pspio_error_free();
}

void error_teardown(void)
{
  pspio_error_free();
}

START_TEST(test_error_fetchall)
{
  ck_assert(pspio_error_add(PSPIO_EVALUE, "test_1_1.c", 1234, "dummy1") == PSPIO_EVALUE);
  err_str = pspio_error_fetchall();
  ck_assert_str_eq(err_str, "libpspio: ERROR:\n"
          "  * in test_1_1.c(dummy1):1234:\n"
          "      value error: bad value found (PSPIO_EVALUE)\n");
  free(err_str);
}
END_TEST

START_TEST(test_error_flush)
{
  char buffer[1024];
  FILE *f;
  ck_assert(pspio_error_add(PSPIO_EVALUE, "test_1_1.c", 1234, "dummy1") == PSPIO_EVALUE);
  f = fmemopen(buffer, sizeof(buffer), "w");
  pspio_error_flush(f);
  fclose(f);
  ck_assert_str_eq(buffer, "libpspio: ERROR:\n"
          "  * in test_1_1.c(dummy1):1234:\n"
          "      value error: bad value found (PSPIO_EVALUE)\n");
}
END_TEST

START_TEST(test_error_string)
{
  ck_assert_str_eq(pspio_error_string(PSPIO_SUCCESS), "success (PSPIO_SUCCESS)");
  ck_assert_str_eq(pspio_error_string(PSPIO_ERROR), "error (PSPIO_ERROR)");
  ck_assert_str_eq(pspio_error_string(PSPIO_EFILE_CORRUPT), "file corrupted (PSPIO_EFILE_CORRUPT)");
  ck_assert_str_eq(pspio_error_string(PSPIO_EFILE_FORMAT), "unknown file format (PSPIO_EFILE_FORMAT)");
  ck_assert_str_eq(pspio_error_string(PSPIO_EIO), "input/output error (PSPIO_EIO)");
  ck_assert_str_eq(pspio_error_string(PSPIO_ENOFILE), "file does not exist (PSPIO_ENOFILE)");
  ck_assert_str_eq(pspio_error_string(PSPIO_ENOMEM), "malloc failed (PSPIO_ENOMEM)");
  ck_assert_str_eq(pspio_error_string(PSPIO_ENOSUPPORT), "unsupported option in the pseudopotential file (PSPIO_ENOSUPPORT)");
  ck_assert_str_eq(pspio_error_string(PSPIO_EVALUE), "value error: bad value found (PSPIO_EVALUE)");
  ck_assert_str_eq(pspio_error_string(-999), "unknown error code");
}
END_TEST

START_TEST(test_error_show)
{
  pid_t pid;
  int fd[2];

  ck_assert(pipe(fd) >= 0);

  pid = fork();
  ck_assert(pid >= 0);
  if (pid > 0) {
    /* Child case. */
    char buffer[1024];
    size_t offset = 0;
    size_t ln;
    close(fd[1]);
    while (offset < sizeof(buffer) && (ln = read(fd[0], buffer + offset, sizeof(buffer) - offset)) > 0) {
        offset += ln;
    }
    buffer[offset] = '\0';
    ck_assert_str_eq(buffer, "libpspio: ERROR:\n  * in test_1_1.c(dummy1):1234:\n      value error: bad value found (PSPIO_EVALUE)\n");
    close(fd[0]);
  } else if (!pid) {
    /* Parent case. */
    close(STDERR_FILENO);
    dup2(fd[1], STDERR_FILENO);
    pspio_error_show(PSPIO_EVALUE, "test_1_1.c", 1234, "dummy1");
    close(fd[0]);
    close(fd[1]);
  }
}
END_TEST

START_TEST(test_error_empty)
{
  ck_assert_int_eq(pspio_error_get_last(NULL), PSPIO_SUCCESS);
  ck_assert_int_eq(pspio_error_len(), 0);

  err_str = pspio_error_fetchall();
  ck_assert(err_str == NULL);
  free(err_str);

  ck_assert_int_eq(pspio_error_get_last(NULL), PSPIO_SUCCESS);
  ck_assert_int_eq(pspio_error_len(), 0);

}
END_TEST

START_TEST(test_error_pop)
{
  pspio_error_t *err;
  ck_assert_int_eq(pspio_error_get_last(NULL), PSPIO_SUCCESS);
  ck_assert_int_eq(pspio_error_len(), 0);

  ck_assert(pspio_error_add(PSPIO_EVALUE, "test_1_1.c", 1234, "dummy1") == PSPIO_EVALUE);
  ck_assert_int_eq(pspio_error_len(), 1);
  err = pspio_error_pop();
  ck_assert(err);
  pspio_error_free_one(err);

  ck_assert_int_eq(pspio_error_get_last(NULL), PSPIO_SUCCESS);
  ck_assert_int_eq(pspio_error_len(), 0);

}
END_TEST

START_TEST(test_error_single)
{
  ck_assert(pspio_error_add(PSPIO_EVALUE, "test_1_1.c", 1234, "dummy1") == PSPIO_EVALUE);
  ck_assert(pspio_error_get_last(NULL) == PSPIO_EVALUE);
  ck_assert(pspio_error_len() == 1);

  err_str = pspio_error_fetchall();
  ck_assert_str_eq(err_str, "libpspio: ERROR:\n"
          "  * in test_1_1.c(dummy1):1234:\n"
          "      value error: bad value found (PSPIO_EVALUE)\n");
  free(err_str);

  ck_assert(pspio_error_get_last(NULL) == PSPIO_SUCCESS);
  ck_assert(pspio_error_len() == 0);
}
END_TEST

START_TEST(test_error_double)
{
  ck_assert(pspio_error_add(PSPIO_EVALUE, "test_2_1.c", 1234, "dummy21") == PSPIO_EVALUE);
  ck_assert(pspio_error_get_last(NULL) == PSPIO_EVALUE);
  ck_assert(pspio_error_len() == 1);

  ck_assert(pspio_error_add(PSPIO_ENOSUPPORT, "test_2_2.c", 202, "dummy22") == PSPIO_ENOSUPPORT);
  ck_assert(pspio_error_get_last(NULL) == PSPIO_ENOSUPPORT);
  ck_assert(pspio_error_len() == 2);

  err_str = pspio_error_fetchall();
  ck_assert_str_eq(err_str, "libpspio: ERROR:\n"
          "  * in test_2_1.c(dummy21):1234:\n"
          "      value error: bad value found (PSPIO_EVALUE)\n"
          "  * in test_2_2.c(dummy22):202:\n"
          "      unsupported option in the pseudopotential file (PSPIO_ENOSUPPORT)\n");
  free(err_str);

  ck_assert(pspio_error_get_last(NULL) == PSPIO_SUCCESS);
  ck_assert(pspio_error_len() == 0);
}
END_TEST

START_TEST(test_error_triple)
{
  char result[PSPIO_STRLEN_ERROR];

  ck_assert(pspio_error_add(PSPIO_EVALUE, "test_3_1.c", 311, "dummy31") == PSPIO_EVALUE);
  ck_assert(pspio_error_get_last(NULL) == PSPIO_EVALUE);
  ck_assert(pspio_error_len() == 1);

  ck_assert(pspio_error_add(PSPIO_ENOFILE, "test_3_2.c", 322, "dummy32") == PSPIO_ENOFILE); 
  ck_assert(pspio_error_get_last(NULL) == PSPIO_ENOFILE);
  ck_assert(pspio_error_len() == 2);

  ck_assert(pspio_error_add(PSPIO_ERROR, "test_3_3.c", 333, "dummy33") == PSPIO_ERROR); 
  ck_assert(pspio_error_get_last(NULL) == PSPIO_ERROR);
  ck_assert(pspio_error_len() == 3);

  sprintf(result, "libpspio: ERROR:\n");
  sprintf(result+strlen(result), "  * in test_3_1.c(dummy31):311:\n"
          "      value error: bad value found (PSPIO_EVALUE)\n");
  sprintf(result+strlen(result), "  * in test_3_2.c(dummy32):322:\n"
          "      file does not exist (PSPIO_ENOFILE)\n");
  sprintf(result+strlen(result), "  * in test_3_3.c(dummy33):333:\n"
          "      error (PSPIO_ERROR)\n");
  err_str = pspio_error_fetchall();
  ck_assert_str_eq(err_str, result);
  free(err_str);

  ck_assert(pspio_error_get_last(NULL) == PSPIO_SUCCESS);
  ck_assert(pspio_error_len() == 0);
}
END_TEST

START_TEST(test_error_get_last)
{
  ck_assert(pspio_error_add(PSPIO_ENOFILE, "test_4_2.c", 422, "dummy42") == PSPIO_ENOFILE);
  ck_assert(pspio_error_add(PSPIO_ERROR,   "test_4_3.c", 433, "dummy43") == PSPIO_ERROR);
  ck_assert(pspio_error_len() == 2);

  ck_assert(pspio_error_get_last("dummy42") == PSPIO_ENOFILE);
  ck_assert(pspio_error_get_last("dummy43") == PSPIO_ERROR);
  ck_assert(pspio_error_get_last("dummy44") == PSPIO_SUCCESS);
}
END_TEST


Suite * make_error_suite(void)
{
  Suite *s;
  TCase *tc_fetch, *tc_empty, *tc_single, *tc_double, *tc_triple, *tc_last;

  s = suite_create("Error");

  tc_fetch = tcase_create("Stringification");
  tcase_add_checked_fixture(tc_fetch, error_setup, error_teardown);
  tcase_add_test(tc_fetch, test_error_fetchall);
  tcase_add_test(tc_fetch, test_error_flush);
  tcase_add_test(tc_fetch, test_error_string);
  tcase_add_test(tc_fetch, test_error_show);
  suite_add_tcase(s, tc_fetch);

  tc_empty = tcase_create("Empty chain");
  tcase_add_checked_fixture(tc_empty, error_setup, error_teardown);
  tcase_add_test(tc_empty, test_error_empty);
  tcase_add_test(tc_empty, test_error_pop);
  suite_add_tcase(s, tc_empty);

  tc_single = tcase_create("Single error chain");
  tcase_add_checked_fixture(tc_single, error_setup, error_teardown);
  tcase_add_test(tc_single, test_error_single);
  suite_add_tcase(s, tc_single);

  tc_double = tcase_create("Double error chain");
  tcase_add_checked_fixture(tc_double, error_setup, error_teardown);
  tcase_add_test(tc_double, test_error_double);
  suite_add_tcase(s, tc_double);

  tc_triple = tcase_create("Triple error chain");
  tcase_add_checked_fixture(tc_triple, error_setup, error_teardown);
  tcase_add_test(tc_triple, test_error_triple);
  suite_add_tcase(s, tc_triple);

  tc_last = tcase_create("Get last");
  tcase_add_checked_fixture(tc_last, error_setup, error_teardown);
  tcase_add_test(tc_last, test_error_get_last);
  suite_add_tcase(s, tc_last);

  return s;
}
